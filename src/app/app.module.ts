import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule,
		MatCheckboxModule,
		MatInputModule,
		MatListModule,
		MatProgressSpinnerModule,
		MatIconModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { SessionService } from './service/session.service';
import { UserService } from './service/user.service';
import { ChatroomService } from './service/chatroom.service';
import { AuthenticationGuardService } from './service/authentication-guard.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ChatroomsComponent } from './components/chatrooms/chatrooms.component';
import { LeftNavigationComponent } from './components/left-navigation/left-navigation.component';
import { LeftNavigationHeaderComponent } from './components/left-navigation-header/left-navigation-header.component';
import { MenuItemsComponent } from './components/menu-items/menu-items.component';
import { RoomHeaderComponent } from './components/room-header/room-header.component';
import { MessagesPanelComponent } from './components/messages-panel/messages-panel.component';
import { MessageBoxComponent } from './components/message-box/message-box.component';

const appRoutes: Routes = [
	{ path: '', component: LoginComponent },
	{ path: 'Chatrooms', component: ChatroomsComponent, canActivate: [AuthenticationGuardService] }
];

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		ChatroomsComponent,
		LeftNavigationComponent,
		LeftNavigationHeaderComponent,
		MenuItemsComponent,
		RoomHeaderComponent,
		MessagesPanelComponent,
		MessageBoxComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(appRoutes, { enableTracing: true }),
		FormsModule,
		HttpClientModule,
		MatProgressSpinnerModule,
		MatButtonModule,
		MatCheckboxModule,
		MatInputModule,
		MatListModule,
		MatIconModule,
		BrowserAnimationsModule
	],
	providers: [
		SessionService,
		UserService,
		ChatroomService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
