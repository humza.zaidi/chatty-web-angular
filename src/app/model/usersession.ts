export interface IUserSession {
	isAuthenticated: boolean;
	username: string;
	loginTime: Date;
}
