export interface IRoomDetails {
	id: number;
	name: string;
	users: string[];
}
