export interface IMessages {
	id: string;
	message: string;
	name: string;
}
