import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-message-box',
	templateUrl: './message-box.component.html',
	styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit {
	message = '';
	constructor() { }
	@Output() sendMessage = new EventEmitter<string>();
	ngOnInit() {
	}

	onMessageSend(message: string): void {
		this.sendMessage.emit(message);
		this.message = '';
	}
}
