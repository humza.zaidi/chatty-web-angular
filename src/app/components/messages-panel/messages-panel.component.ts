import { Component, OnInit, Input } from '@angular/core';
import { IMessages } from '../../model/imessages';
import { UserService } from '../../service/user.service';
import { IUserSession } from '../../model/usersession';

@Component({
	selector: 'app-messages-panel',
	templateUrl: './messages-panel.component.html',
	styleUrls: ['./messages-panel.component.css']
})
export class MessagesPanelComponent implements OnInit {

	loggedInUser: IUserSession;
	private _messages: IMessages[] = [];
	@Input() loading: boolean;
	@Input()
	set messages(messages: IMessages[]) {
		if (messages) {
			this._messages = messages.reverse();
		}
	}
	get messages(): IMessages[] {
		return this._messages;
	}
	constructor(private userService: UserService) { }

	ngOnInit() {
		this.loggedInUser = this.userService.getUsersession();
	}

}
