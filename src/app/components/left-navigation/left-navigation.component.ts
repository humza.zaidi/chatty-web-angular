import { Component, OnInit, Input } from '@angular/core';
import { IRooms } from '../../model/irooms';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';

@Component({
	selector: 'app-left-navigation',
	templateUrl: './left-navigation.component.html',
	styleUrls: ['./left-navigation.component.css']
})
export class LeftNavigationComponent implements OnInit {

	@Input() rooms: IRooms[];
	@Input() loading: boolean;
	constructor(private userService: UserService, private router: Router) { }

	ngOnInit() {
	}

	onLogoutClick() {
		this.userService.logout();
		this.router.navigate(['/']);
	}

}
