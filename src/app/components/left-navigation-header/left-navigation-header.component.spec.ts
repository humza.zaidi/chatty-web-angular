import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavigationHeaderComponent } from './left-navigation-header.component';

describe('LeftNavigationHeaderComponent', () => {
  let component: LeftNavigationHeaderComponent;
  let fixture: ComponentFixture<LeftNavigationHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavigationHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavigationHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
