import { Component, OnInit,  OnDestroy  } from '@angular/core';
import { UserService } from '../../service/user.service';
import { IUserSession } from '../../model/usersession';

@Component({
	selector: 'app-left-navigation-header',
	templateUrl: './left-navigation-header.component.html',
	styleUrls: ['./left-navigation-header.component.css']
})
export class LeftNavigationHeaderComponent implements OnInit,  OnDestroy  {
	private loggedInUser: IUserSession;
	username: string;
	loginSince: string;
	interval: any;
	constructor(private userService: UserService) { }

	private _loginSinceText(now: Date , loggedInTime: Date): string {
		let diffInMilliseconts: number;
		let timeInMinutes: number;
		diffInMilliseconts = now.getTime() - loggedInTime.getTime();
		timeInMinutes = Math.floor(diffInMilliseconts / 1000 / 60);
		if (timeInMinutes === 0) {
			return 'Just logged in';
		} else if (timeInMinutes === 1) {
			return 'Online for 1 minute';
		} else {
			return `Online for ${timeInMinutes} minutes`;
		}
	}

	ngOnInit() {
		this.loggedInUser = this.userService.getUsersession();
		this.username = this.loggedInUser.username;
		this.loginSince = this._loginSinceText(new Date(), this.loggedInUser.loginTime);
		this.interval = setInterval(() => {
			this.loginSince = this._loginSinceText(new Date(), this.loggedInUser.loginTime);
		}, 1000);
	}

	ngOnDestroy() {
		clearInterval(this.interval);
	}

}
