import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username = '';
	constructor(private userService: UserService, private router: Router) {

	}

	ngOnInit() {
	}

	onLoginClick(username: string) {
		this.userService.login(username);
		this.router.navigate(['/Chatrooms']);
	}

}
