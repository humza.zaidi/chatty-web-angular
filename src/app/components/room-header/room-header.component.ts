import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../service/user.service';
import { IUserSession } from '../../model/usersession';

@Component({
	selector: 'app-room-header',
	templateUrl: './room-header.component.html',
	styleUrls: ['./room-header.component.css']
})
export class RoomHeaderComponent implements OnInit {
	private _users: Set<string>;
	@Input() roomName: string;
	@Input() loading: boolean;
	@Input()
	set users(users: string[]) {
		this._users = new Set(users);
	}
	get users(): string[] {
		const result: string[] = [];
		this._users.forEach(user => {
			result.push(user);
		});
		if (!result.includes(this.usersession.username)) {
			result.unshift(this.usersession.username);
		}
		return result;
	}
	usersession: IUserSession;
	constructor(private userService: UserService) { }

	ngOnInit() {
		this.usersession = this.userService.getUsersession();
	}




}
