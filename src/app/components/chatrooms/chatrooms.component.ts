import { Component, OnInit } from '@angular/core';
import { ChatroomService } from '../../service/chatroom.service';
import { IRooms } from '../../model/irooms';
import { IRoomDetails } from '../../model/iroom-details';
import { IMessages } from '../../model/imessages';
import { forkJoin } from 'rxjs';
import { UserService } from '../../service/user.service';

@Component({
	selector: 'app-chatrooms',
	templateUrl: './chatrooms.component.html',
	styleUrls: ['./chatrooms.component.css']
})
export class ChatroomsComponent implements OnInit {
	loadingRooms = true;
	loadingRoomDetails = true;
	loadingMessages = true;
	rooms: IRooms[];
	roomDetails: IRoomDetails;
	messages: IMessages[];
	constructor(private chatroomService: ChatroomService, private userService: UserService) { }

	ngOnInit() {
		this.chatroomService.getRooms()
			.subscribe((data: IRooms[]) => {
				this.rooms = data;
				this.loadingRooms = false;
				const firstRoom = this.rooms[0];
				forkJoin([ this.chatroomService.getRoomDetails(firstRoom.id), this.chatroomService.getMessagesByRoom(firstRoom.id) ])
					.subscribe(([ roomDetails, messages ]: [ IRoomDetails, IMessages[]]) => {
						this.loadingRoomDetails = this.loadingMessages = false;
						this.roomDetails = roomDetails;
						this.messages = messages;
					});
			});
	}

	onSendMessage(message: string): void {
		this.chatroomService.postMessage(this.roomDetails.id, this.userService.getUsername(), message)
			.subscribe((data: IMessages) => {
				this.messages.unshift(data);
			});
	}

}
