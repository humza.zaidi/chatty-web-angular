import { Component, OnInit, Input } from '@angular/core';
import { IRooms } from '../../model/irooms';

@Component({
	selector: 'app-menu-items',
	templateUrl: './menu-items.component.html',
	styleUrls: ['./menu-items.component.css']
})
export class MenuItemsComponent implements OnInit {

	@Input() rooms: IRooms[];
	@Input() loading: boolean;
	constructor() { }

	ngOnInit() {
	}

}
