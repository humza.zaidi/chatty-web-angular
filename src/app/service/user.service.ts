import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { IUserSession } from '../model/usersession';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	authentication: IUserSession;
	constructor(private sessionService: SessionService) {
		this.authentication = {
			isAuthenticated: false,
			username: '',
			loginTime: null
		};
	}
	private _setAuthentication(username: string): void {
		this.authentication = {
			isAuthenticated: true,
			username,
			loginTime: new Date()
		};
	}

	login(username: string): void {
		this._setAuthentication(username);
		this.sessionService.setItem('username', username);
	}

	logout(): void {
		this.sessionService.remove('username');
	}

	isAuthenticated(): boolean {
		return this.authentication.isAuthenticated;
	}

	getUsername(): string {
		return this.authentication.username;
	}

	getUsersession(): IUserSession {
		return this.authentication;
	}

}
