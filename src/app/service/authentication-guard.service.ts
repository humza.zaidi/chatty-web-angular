import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

	constructor(private userService: UserService, public router: Router) { }
	canActivate(): boolean {
		if (!this.userService.isAuthenticated()) {
			this.router.navigate(['/']);
			return false;
		}
		return true;
	}
}
