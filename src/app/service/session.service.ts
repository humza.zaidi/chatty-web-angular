import { Injectable } from '@angular/core';
import { KeyEventsPlugin } from '@angular/platform-browser/src/dom/events/key_events';

@Injectable({
	providedIn: 'root'
})
export class SessionService {

	constructor() { }

	setItem(key: string, value: string): void {
		sessionStorage.setItem(key, value);
	}
	remove(key: string): void {
		sessionStorage.removeItem(key);
	}

}
