import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMessages } from '../model/imessages';


@Injectable({
	providedIn: 'root'
})
export class ChatroomService {
	private baseUrl = 'https://frontend-project-sygmaduaqi.now.sh/api';
	constructor(private http: HttpClient) { }

	getRooms() {
		return this.http.get(`${this.baseUrl}/rooms`);
	}

	getRoomDetails(id: number) {
		return this.http.get(`${this.baseUrl}/rooms/${id}`);
	}

	getMessagesByRoom(id: number) {
		return this.http.get(`${this.baseUrl}/rooms/${id}/messages`);
	}

	postMessage(id, username, message): Observable<IMessages> {
		return this.http.post<IMessages>(`${this.baseUrl}/rooms/${id}/messages`, { name: username, message });
	}

}
